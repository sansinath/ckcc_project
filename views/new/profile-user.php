<?php include "./part/head.php" ?>
<link rel="stylesheet" href="profile-user.css">
<title>Personal</title>

</head>

<body>
    <div id="main-wrapper">
        <div id="header" style="box-shadow: 0 1px 2px 0 rgba(0,0,0,.25)">
            <?php include "./part/header.php"?>

        </div>
        <div class="cover">

        </div>

        <!-- <div class="main-content">
            <div class="base-row-area">
                <div class="personal">
                    <div class="" style="background-color: #FAFAFA">
                        <img src="../image/vanna.jpg" alt="" width="10%">
                        <span>Heang Vanna</span>
                        <ul>
                                <li><a href="">prototype</a></li>
                                <li><a href="">Design</a></li>
                                <li><a href="">Software</a></li>
                                <li><a href="">Logo</a></li>
                        </ul>
                       
                        <form class="frm-group" action="">     
                                <label for=""><img src="../image/search.png" alt=""></label>
                                    <input type="search" class="search-field">
                        </form>
                    </div>
                </div>
            </div>
        </div>
     -->

        <!-- <div class="base-row-right"> 
                <div class="side-right">
                    <div class="sub-side-r-1">
                                <div class="menu">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                                <div class="menu-bar">
                                    <ul>
                                        <li><a href="">home</a></li>
                                        <li><a href="">home</a></li>
                                        <li><a href="">home</a></li>
                                        <li><a href="">home</a></li>
                                        <li><a href="">home</a></li>
                                    </ul>
                                </div>
                                <script>
                                    let a=0;
                                    $(document).ready(function(){
                                        
                                        $('.menu').click(function(){
                                                $('.menu').toggleClass('active');
                                               
                                                if(a==1){
                                                    $('.menu-bar').hide();
                                                    
                                                    a=0;
                                                }else if(a==0){
                                                    $('.menu-bar').show('1000s');
                                                   
                                                    a=1;
                                                }
                                                

                                        })
                                    })
                                </script>
                    </div>
                </div>
            </div>  -->
        <div class="profile">
            <div class="pro" style="background-color: #FAFAFA">
                <div class="pro-profile">

                    <img src="../image/profile.jpg" alt="" width="7%">
                    <span>Heang Vanna</span>
                </div>
                <div class="pro-nav">
                    <ul>
                        <li><a href="">Account</a></li>
                        <li><a href="">Post</a></li>
                        <li><a href="">About</a></li>
                    </ul>
                </div>
                <div class="pro-search ">
                    <form class="frm-group" action="">

                        <input type="search" class="search-field" id="search-field">
                    </form>
                </div>

            </div>
        </div>

        <div class="base-row-area">
            <div class="p-aside border-right">
                <ul>
                    <li><a href="">Home</a></li>
                    <!-- <li><a href="">All Post</a></li> -->
                    <li><a href="">All Post</a></li>
                    <li><a href="">Recently Post </a></li>
                    <li><a href="">Most Rated</a></li>
                    <li><a href="">Most View</a></li>
                    <li><a href="">Setting</a></li>
                    <li><a href="">Setting</a></li>
                </ul>
            </div>
            <div class="p-content ">
                <section>
                    <div class="p-profile-view-header">
                        <h2>Recently Post All <span id="add-project">Post Projects</span><span></span></h2>
                    </div>
                    <div style="width:92%;margin:0 auto;">
                        <article>
                            <div class="box-item ">
                                <div class="flex">
                                    <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                        <div class="item">
                                            <img src="../image/c.jpeg" alt="" class="item-ad-img">
                                            <div class="item-profile">
                                                <div class="photo-user">
                                                    <span class="user-img">
                                                        <img src="../image/vanna.jpg">
                                                    </span>
                                                    <span class="username">
                                                        Heang Vanna
                                                    </span>
                                                </div>
                                                <h3 class="item-descript">I will design responsive and
                                                    prfessional
                                                    websites. I will create web app development ...</h3>
                                                <div class="item-love">
                                                    <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="2"
                                                        height="2">
                                                        <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">
                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                                    <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8"
                                                        width="16" height="16">
                                                        <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                                    <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16"
                                                        height="16">
                                                        <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">
                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                                </div>
                                            </div>
                                            <footer class="item-footer">
                                                <div class="price">
                                                    <span>
                                                        <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792"
                                                            width="15" height="15">
                                                            <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                        </svg>
                                                        <span>
                                                            <strong>5.0</strong>
                                                            (62)
                                                        </span>
                                                    </span>
                                                    <small style="color:#34A853">
                                                        Starting At
                                                    </small>

                                                    <small style="color:#FA3E3E"> $222</small>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                    <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                        <div class="item">
                                            <img src="../image/b8.jpeg" alt="" class="item-ad-img">
                                            <div class="item-profile">
                                                <div class="photo-user">
                                                    <span class="user-img">
                                                        <img src="../image/vanna.jpg">
                                                    </span>
                                                    <span class="username">
                                                        Heang Vanna
                                                    </span>
                                                </div>
                                                <h3 class="item-descript">I will design responsive and
                                                    prfessional websites. I will create web app development ...</h3>
                                                <div class="item-love">
                                                    <svg id="eye" viewBox="0 0 30.5 16.5" width="16" height="16">
                                                        <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">
                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                                    <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8"
                                                        width="16" height="16">
                                                        <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                                    <svg id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                        <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">
                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                                </div>
                                            </div>
                                            <footer class="item-footer">
                                                <div class="price">
                                                    <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                        height="15">
                                                        <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                    </svg>
                                                    <span>
                                                        <strong>5.0</strong>
                                                        (62)
                                                    </span>

                                                    <small style="color:#34A853">
                                                        Starting At
                                                    </small>

                                                    <small style="color:#FA3E3E"> $222</small>

                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                    <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                        <div class="item">
                                            <img src="../image/d3.jpg" alt="" class="item-ad-img">

                                            <div class="item-profile">
                                                <div class="photo-user">
                                                    <span class="user-img">
                                                        <img src="../image/vanna.jpg">
                                                    </span>
                                                    <span class="username">
                                                        Heang Vanna
                                                    </span>
                                                </div>

                                                <h3 class="item-descript">I will design responsive and
                                                    prfessional websites. I will create web app development ...</h3>


                                                <div class="item-love">

                                                    <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                        height="16">
                                                        <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                        </path>

                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                    <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8"
                                                        width="16" height="16">
                                                        <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                    <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16"
                                                        height="16">
                                                        <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                </div>
                                            </div>

                                            <footer class="item-footer">
                                                <div class="price">

                                                    <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                        height="15">
                                                        <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                    </svg>
                                                    <span>
                                                        <strong>5.0</strong>
                                                        (62)
                                                    </span>

                                                    <small style="color:#34A853">
                                                        Starting At
                                                    </small>

                                                    <small style="color:#FA3E3E"> $222</small>

                                                </div>
                                            </footer>

                                        </div>
                                    </div>
                                    <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                        <div class="item">
                                            <img src="../image/c5.jpeg" alt="" class="item-ad-img">

                                            <div class="item-profile">
                                                <div class="photo-user">
                                                    <span class="user-img">
                                                        <img src="../image/vanna.jpg">
                                                    </span>
                                                    <span class="username">
                                                        Heang Vanna
                                                    </span>
                                                </div>

                                                <h3 class="item-descript">I will design responsive and
                                                    prfessional websites. I will create web app development ...</h3>


                                                <div class="item-love">

                                                    <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                        height="16">
                                                        <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                        </path>

                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                    <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8"
                                                        width="16" height="16">
                                                        <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                    <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16"
                                                        height="16">
                                                        <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                </div>
                                            </div>

                                            <footer class="item-footer">
                                                <div class="price">

                                                    <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                        height="15">
                                                        <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                    </svg>
                                                    <span>
                                                        <strong>5.0</strong>
                                                        (62)
                                                    </span>

                                                    <small style="color:#34A853">
                                                        Starting At
                                                    </small>

                                                    <small style="color:#FA3E3E"> $222</small>


                                                </div>
                                            </footer>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="p-profile-more">
                        <h2>See more ...</h2>
                    </div>
                    <div class="p-profile-view-header">
                        <h2>Recently Post All</h2>
                    </div>
                    <div style="width:92%;margin:0 auto;">
                        <article>
                            <div class="box-item ">
                                <div class="flex">
                                    <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                        <div class="item">
                                            <img src="../image/c.jpeg" alt="" class="item-ad-img">
                                            <div class="item-profile">
                                                <div class="photo-user">
                                                    <span class="user-img">
                                                        <img src="../image/vanna.jpg">
                                                    </span>
                                                    <span class="username">
                                                        Heang Vanna
                                                    </span>
                                                </div>
                                                <h3 class="item-descript">I will design responsive and
                                                    prfessional
                                                    websites. I will create web app development ...</h3>
                                                <div class="item-love">
                                                    <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="2"
                                                        height="2">
                                                        <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">
                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                                    <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8"
                                                        width="16" height="16">
                                                        <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                                    <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16"
                                                        height="16">
                                                        <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">
                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                                </div>
                                            </div>
                                            <footer class="item-footer">
                                                <div class="price">
                                                    <span>
                                                        <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792"
                                                            width="15" height="15">
                                                            <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                        </svg>
                                                        <span>
                                                            <strong>5.0</strong>
                                                            (62)
                                                        </span>
                                                    </span>
                                                    <small style="color:#34A853">
                                                        Starting At
                                                    </small>

                                                    <small style="color:#FA3E3E"> $222</small>
                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                    <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                        <div class="item">
                                            <img src="../image/b8.jpeg" alt="" class="item-ad-img">
                                            <div class="item-profile">
                                                <div class="photo-user">
                                                    <span class="user-img">
                                                        <img src="../image/vanna.jpg">
                                                    </span>
                                                    <span class="username">
                                                        Heang Vanna
                                                    </span>
                                                </div>
                                                <h3 class="item-descript">I will design responsive and
                                                    prfessional websites. I will create web app development ...</h3>
                                                <div class="item-love">
                                                    <svg id="eye" viewBox="0 0 30.5 16.5" width="16" height="16">
                                                        <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">
                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                                    <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8"
                                                        width="16" height="16">
                                                        <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                                    <svg id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                        <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">
                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                                </div>
                                            </div>
                                            <footer class="item-footer">
                                                <div class="price">
                                                    <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                        height="15">
                                                        <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                    </svg>
                                                    <span>
                                                        <strong>5.0</strong>
                                                        (62)
                                                    </span>

                                                    <small style="color:#34A853">
                                                        Starting At
                                                    </small>

                                                    <small style="color:#FA3E3E"> $222</small>

                                                </div>
                                            </footer>
                                        </div>
                                    </div>
                                    <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                        <div class="item">
                                            <img src="../image/d3.jpg" alt="" class="item-ad-img">

                                            <div class="item-profile">
                                                <div class="photo-user">
                                                    <span class="user-img">
                                                        <img src="../image/vanna.jpg">
                                                    </span>
                                                    <span class="username">
                                                        Heang Vanna
                                                    </span>
                                                </div>

                                                <h3 class="item-descript">I will design responsive and
                                                    prfessional websites. I will create web app development ...</h3>


                                                <div class="item-love">

                                                    <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                        height="16">
                                                        <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                        </path>

                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                    <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8"
                                                        width="16" height="16">
                                                        <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                    <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16"
                                                        height="16">
                                                        <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                </div>
                                            </div>

                                            <footer class="item-footer">
                                                <div class="price">

                                                    <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                        height="15">
                                                        <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                    </svg>
                                                    <span>
                                                        <strong>5.0</strong>
                                                        (62)
                                                    </span>

                                                    <small style="color:#34A853">
                                                        Starting At
                                                    </small>

                                                    <small style="color:#FA3E3E"> $222</small>

                                                </div>
                                            </footer>

                                        </div>
                                    </div>
                                    <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                        <div class="item">
                                            <img src="../image/c5.jpeg" alt="" class="item-ad-img">

                                            <div class="item-profile">
                                                <div class="photo-user">
                                                    <span class="user-img">
                                                        <img src="../image/vanna.jpg">
                                                    </span>
                                                    <span class="username">
                                                        Heang Vanna
                                                    </span>
                                                </div>

                                                <h3 class="item-descript">I will design responsive and
                                                    prfessional websites. I will create web app development ...</h3>


                                                <div class="item-love">

                                                    <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                        height="16">
                                                        <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                        </path>

                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                    <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8"
                                                        width="16" height="16">
                                                        <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                    <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16"
                                                        height="16">
                                                        <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                        </path>
                                                    </svg>
                                                    <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                                </div>
                                            </div>

                                            <footer class="item-footer">
                                                <div class="price">

                                                    <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                        height="15">
                                                        <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                    </svg>
                                                    <span>
                                                        <strong>5.0</strong>
                                                        (62)
                                                    </span>

                                                    <small style="color:#34A853">
                                                        Starting At
                                                    </small>

                                                    <small style="color:#FA3E3E"> $222</small>


                                                </div>
                                            </footer>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="p-profile-more">
                        <h2>See more ...</h2>
                    </div>


                </section>
                <div style="width: 100%;margin:0 auto;display: block;">
                    <div style="width: 100%;height: 44px; position: relative;box-sizing: border-box;padding-left: 77px;;margin: 0;">
                        <h2 style="border-bottom: 1.50px solid #b3bead;color: rgb(83, 87, 216); display: inline-block;">People Contact</h2>
                    </div>
                    <div style="position: relative; max-width: 100%;height:66px;overflow: hidden">

                        <div style="position: absolute;top:8%;right: 10%">
                            <span style="text-align: right">
                                <p> All</p>
                            </span>
                        </div>
                        <div style="position: absolute;top: 0;right:5.50%">
                            <div class="ondisplay" style="text-align: right">
                                <section title=".squaredFour">

                                    <div class="squaredFour">
                                        <input type="checkbox" value="None" id="squaredFour" name="check" checked />
                                        <label for="squaredFour"></label>
                                    </div>

                                </section>

                            </div>
                        </div>

                    </div>
                    <div class="people-contact border-bottom">
                        <div class="contact-td-1">
                            <img src="../image/profile.jpg" alt="" width="7%">
                            <span>Heang Vanna</span>
                            <p>Date: 22 December 2018</p>
                        </div>

                        <div class="contact-td-2">Logo Design</div>
                        <div class="contact-td-3">7</div>
                        <div class="contact-td-4">heangvanna@gmail.com</div>
                        <div class="contact-td-5">
                            <div class="ondisplay">
                                <section title=".squaredFour">
                                    <!-- .squaredFour -->
                                    <div class="squaredFour">
                                        <input type="checkbox" value="None" id="squaredFour" name="check" checked />
                                        <label for="squaredFour"></label>
                                    </div>
                                    <!-- end .squaredFour -->
                                </section>

                            </div>

                        </div>

                    </div>
                    <div class="people-contact border-bottom">
                        <div class="contact-td-1">
                            <img src="../image/profile.jpg" alt="" width="7%">
                            <span>Heang Vanna</span>
                            <p>Date: 22 December 2018</p>
                        </div>

                        <div class="contact-td-2">Logo Design</div>
                        <div class="contact-td-3">7</div>
                        <div class="contact-td-4">heangvanna@gmail.com</div>
                        <div class="contact-td-5">
                            <div class="ondisplay">
                                <section title=".squaredFour">

                                    <div class="squaredFour">
                                        <input type="checkbox" value="None" id="squaredFour" name="check" checked />
                                        <label for="squaredFour"></label>
                                    </div>

                                </section>

                            </div>

                        </div>

                    </div>
                    <div class="people-contact border-bottom">
                        <div class="contact-td-1">
                            <img src="../image/sovann.jpg" alt="" width="7%">
                            <span>Heang Vanna</span>
                            <p>Date: 22 December 2018</p>
                        </div>

                        <div class="contact-td-2">Logo Design</div>
                        <div class="contact-td-3">7</div>
                        <div class="contact-td-4">heangvanna@gmail.com</div>
                        <div class="contact-td-5">
                            <div class="ondisplay">
                                <section title=".squaredFour">
                                    <!-- .squaredFour -->
                                    <div class="squaredFour">
                                        <input type="checkbox" value="None" id="squaredFour" name="check" checked />
                                        <label for="squaredFour"></label>
                                    </div>
                                    <!-- end .squaredFour -->
                                </section>

                            </div>

                        </div>

                    </div>
                    <div class="people-contact border-bottom">
                        <div class="contact-td-1">
                            <img src="../image/profile.jpg" alt="" width="7%">
                            <span>Heang Vanna</span>
                            <p>Date: 22 December 2018</p>
                        </div>

                        <div class="contact-td-2">Logo Design</div>
                        <div class="contact-td-3">7</div>
                        <div class="contact-td-4">heangvanna@gmail.com</div>
                        <div class="contact-td-5">
                            <div class="ondisplay">
                                <section title=".squaredFour">
                                    <!-- .squaredFour -->
                                    <div class="squaredFour">
                                        <input type="checkbox" value="None" id="squaredFour" name="check" checked />
                                        <label for="squaredFour"></label>
                                    </div>
                                    <!-- end .squaredFour -->
                                </section>

                            </div>

                        </div>

                    </div>
                    <div class="people-contact border-bottom">
                        <div class="contact-td-1">
                            <img src="../image/sovann.jpg" alt="" width="7%">
                            <span>Heang Vanna</span>
                            <p>Date: 22 December 2018</p>
                        </div>

                        <div class="contact-td-2">Logo Design</div>
                        <div class="contact-td-3">7</div>
                        <div class="contact-td-4">heangvanna@gmail.com</div>
                        <div class="contact-td-5">
                            <div class="ondisplay">
                                <section title=".squaredFour">
                                    <!-- .squaredFour -->
                                    <div class="squaredFour">
                                        <input type="checkbox" value="None" id="squaredFour" name="check" checked />
                                        <label for="squaredFour"></label>
                                    </div>
                                    <!-- end .squaredFour -->
                                </section>

                            </div>

                        </div>

                    </div>
                    <div class="people-contact border-bottom">
                        <div class="contact-td-1">
                            <img src="../image/profile.jpg" alt="" width="7%">
                            <span>Heang Vanna</span>
                            <p>Date: 22 December 2018</p>
                        </div>

                        <div class="contact-td-2">Logo Design</div>
                        <div class="contact-td-3">7</div>
                        <div class="contact-td-4">heangvanna@gmail.com</div>
                        <div class="contact-td-5">
                            <div class="ondisplay">
                                <section title=".squaredFour">
                                    <!-- .squaredFour -->
                                    <div class="squaredFour">
                                        <input type="checkbox" value="None" id="squaredFour" name="check" checked />
                                        <label for="squaredFour"></label>
                                    </div>
                                    <!-- end .squaredFour -->
                                </section>

                            </div>

                        </div>

                    </div>
                    <div class="btn-contact">
                        <button>Delete</button>
                    </div>
                </div>

            </div>

        </div>
        <!-- <div class="base-row-left">
                <div class="side-left" >
                    <div class="sub-side-f-1 " style="background-color: #FAFAFA">
                        <img src="../image/vanna.jpg" alt="">
                        <span>Heang Vanna</span>
                    </div>
                    <div class="sub-side-f-2">
                        <a href="">Product </a>
                            <div class="next">
                                    <span></span>
                                    <span></span>
                            </div>
                    </div>
                    <div class="sub-side-f-3">
                            <a href="">E-commerce </a>
                            <div class="next">
                                    <span></span>
                                    <span></span>
                            </div>
                    </div>
                    <div class="sub-side-f-4">
                            <a href="">Networking </a>
                            <div class="next">
                                    <span></span>
                                    <span></span>
                            </div>
                    </div>
                    <div class="sub-side-f-5">
                            <a href="">Computer Science </a>
                            <div class="next">
                                    <span></span>
                                    <span></span>
                            </div>
                    </div>
                    <div class="sub-side-f-6">
                            <a href=""> Computer Engineering</a>
                            <div class="next">
                                    <span></span>
                                    <span></span>
                            </div>
                    </div>
                    <div class="sub-side-f-7">
                            <a href="">Data base</a>
                            <div class="next">
                                    <span></span>
                                    <span></span>
                            </div>
                    </div>
                    <div class="sub-side-f-8">
                            <a href="">Software Development </a>
                            <div class="next">
                                    <span></span>
                                    <span></span>
                            </div>
                    </div>
                    <div class="sub-side-f-9">
                            <a href="">Software Development </a>
                            <div class="next">
                                    <span></span>
                                    <span></span>
                            </div>
                    </div>
                    <div class="sub-side-f-10">
                            <a href="">Software Development </a>
                            <div class="next">
                                    <span></span>
                                    <span></span>
                            </div>
                    </div>
                    <div class="sub-side-f-11 active">
                            <a href="">UX / UI Design...</a>
                            
                    </div>

                    
                </div>
            </div>  -->


        <footer>
            <?php include "./part/footer.php" ?>
        </footer>

        <?php include "./part/go-top.php" ?>

    </div>
</body>

</html>