<?php ?>
<?php include "./part/head.php" ?>


    <title>Register</title>
<style>
    
.login-frm-f-r {
    position:relative;
    top: 0;
    left: 0;
    width: 100%;
    height:311px;
    margin: 0 auto;
    box-sizing: border-box;
    z-index:22;
    
}
.login-frm-f-r form  {
    max-width:320px;
    box-sizing: border-box;
    position:relative;
width:80%;
font-size: 32px;
background-color:rgba(255, 255, 255, 0.979);
padding:32px;
border-radius:4px;
/* transform:translate(-50%,50%); */
/* position:absolute; */
top:18%;
/* left:50%; */
/* z-index: 2; */
box-shadow:1px 2px 3px 3px rgba(0,0,0,.1);
/* background:rgba(0,0,0,.9); */
}
.frm-header-r {
    display: block;
   width:100%;
    padding: 0;
}
.frm-header-r span {
    color: #777;
    padding-bottom:22px;
    padding-left:22%;
    display:block;
    font-size: 26px;
}
.login-frm-f-r  form .form-control {
    padding: 0;
    margin: 0;
    display: block;
    width: 100%;
    background:0 0;
    border: none;
    border-bottom:1px solid #999999;
    border-radius: 0; 
    box-shadow: 0;
    outline: 0;
    line-height:2.2;
    color: inherit;
}
.login-frm-f-r .btn-submit {
    width: 100%;
    background-color: #F1F2F3;
    border: none;
    border-radius: 4px;
    padding: 11px;
    box-shadow: 0;
    margin-top: 27px;
    
    text-shadow: none;
    outline: 0;
    cursor: pointer;
}
button[type='submit'].btn-submit  {
    font-size: 17px;
}
 ::placeholder {
        font-size: 14px;
        padding: 11px;
    } 
.login-frm-f-r .forgot {
    display: block;
    text-align: center;
    font-size: 15px;
    color: #6f7a85;
    opacity: .9;
    text-decoration: none;
    cursor: pointer;
    padding-top: 22px;
}
.login-frm-f-r .forgot:hover {
    text-decoration: underline;
    color: rgb(72, 255, 148);

}
.form-group {
    height: calc(2.875rem + 7px);

}
.login-frm-f-r .form-control-connect {
    display: block;
    background:0 0;
    overflow: hidden;
    border: none;
    box-shadow: 0;
    outline: 0;
    line-height:2.7;
    padding:8px;
    margin: 11px;
}
.login-frm-f-r  .form-control-connect a {
    text-decoration: none;
    color: #fff;
    padding: 0;
    margin: 12px;
    left: 0;
}
.login-frm-f-r  .form-control-connect a img {
    width: 10%;
    height: 26px;
    color: #fff;
    float: left;
    
}
.under-solid {
    box-sizing: border-box;
    width:100%;
    overflow: hidden;
    height:28px;
}
.under-solid .solid-1 {
    width:43%;
    height: 20px; float: left; 
    border-bottom: 1px solid #e5e5e5;
}
.under-solid .solid-or {
width: 10%;
height: 20px;
font-size: 18px;
 color: #555; 
 float: left;
 padding-left:12px;
 padding-top: 8px;
}
.under-solid .solid-2 {
    width: 43%;
    height: 20px; 
     border-bottom: 1px solid #e5e5e5;
     float: left;
}
.account-profile{
    position: relative;;
   width: 100%;
   height: 322px;
}
.account-profile img{
    width: 28%;
    border-radius: 50%;
    position: absolute;
    left: 40%;
    top: 30%;
  
}
.account-profile span {
    color: #6f7a85;
    position: absolute;
    left: 38%;
    top: 60%;
    
}
.other-account {
    position: relative;;
   width: 100%;
   height:200px;
}
.other-account img{
    width: 10%;
    border-radius: 50%;
    position: absolute;
    left: 44%;
    top:0;
  
}
.other-account span {
 
    position: absolute;
    left: 40%;
    top: 40%;
    
}
.other-account span a {
color: #6f7a85;
 
}
.other-account button {
 
 position: absolute;
 left: 40%;
 top: 45%;
 
}
.bg-register {
    background-image: url("../image/bg1.jpg");
    background-size: 100%;
}

.other-account .btn-submit {
    width: 20%;
    background-color: rgb(5, 160, 129);
    color: rgb(255, 255, 255);
    border: none;
    border-radius: 4px;
    padding: 11px;
    box-shadow: 0;
    margin-top: 27px;
    
    text-shadow: none;
    outline: 0;
    cursor: pointer;
}
button[type='submit'].btn-submit  {
    font-size: 17px;
}
#pop-up{
            width:37%;
            height: 488px;
          
            position: fixed;
            z-index:99;
            transform: translate(-50%,-50%);
            left:50%;
            top: 50%;
            display: none;
            background-color:rgba(255, 255, 255, 1);
padding:32px;
border-radius:4px;
box-shadow:1px 2px 3px 3px rgba(0,0,0,.1);
        }
        #pop-up #close-btn{
            background-image: url("../image/close-icon.png");
            color: red;
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain;
             width: 35px;
            height: 35px;
            cursor: pointer;
            position: absolute;
            right: 10px;
            top: 10px;
            opacity: 0.5;
        }
        #pop-up  #close-btn:hover{
            opacity: 1;
        }
        #pop-up img {
            width: 36%;
            border-radius: 50%;
            margin-left: 30%;
        }
        #pop-up textarea {
    border: 1px solid #e5e5e5;
    border-radius: 6px;
    text-align: left;
    width: 77%;
    height: 88px;
    margin-left: 28px;


}
#pop-up h2 {
    color: #777777;
    font-size: 22px;
    display: block;
    padding: 22px 0 0 48px;
    
}
#pop-up::placeholder {
 
}
#pop-up textarea {
    outline: none;
    margin-top: 22px;
}
#pop-up button {
    margin-top: 22px;
    margin-left: 77px;
    padding: 11px 16px;
    border: none;
    font-size: 18px;
    border-radius: 4px;
    box-shadow: 0;
    border: 1px solid #6f7a85;
    text-decoration: underline;
    color: #1767A3;
  background-color: #fff;
    text-shadow: none;
    outline: 0;
    cursor: pointer;
}

/* verify */

#pop-up-verify{
    box-sizing: border-box;
            width:37%;
            height: 488px;
          
            position: fixed;
            z-index:99;
            transform: translate(-50%,-50%);
            left:50%;
            top: 50%;
            display: none;
            background-color:rgba(255, 255, 255, 1);
padding:32px;
border-radius:4px;
box-shadow:1px 2px 3px 3px rgba(0,0,0,.1);
        }
        #pop-up-verify #close-btn{
            background-image: url("../image/close-icon.png");
            color: red;
            background-position: center;
            background-repeat: no-repeat;
            background-size: contain;
             width: 35px;
            height: 35px;
            cursor: pointer;
            position: absolute;
            right: 10px;
            top: 10px;
            opacity: 0.5;
        }
        #pop-up-verify #close-btn:hover{
            opacity: 1;
        }
        #pop-up-verify img {
            margin-top: 44px;
            width: 36%;
            border-radius: 50%;
            margin-left: 30%;
        }
      
        #pop-up-verify button {
    margin-top: 35px;
    margin-left: 166px;
    padding: 7px 12px;
    border: none;
    font-size: 12px;
    border-radius: 4px;
    box-shadow: 0;
    color: #e5e5e5;
    border: 1px solid #6f7a85;
  background-color: rgb(167, 161, 161);
    text-shadow: none;
    outline: 0;
    cursor: pointer;
}
#pop-up-verify input {
    border: 1px solid #e5e5e5;
    border-radius: 6px;
    text-align: left;
    width: 66%;
    margin-left: 48px;
    padding: 8px;

}
#pop-up-verify p {
    padding-left: 48px;
    margin-top:12px;
        font-size: 14px;
    color: #777777;
    overflow: hidden;
}

#pop-up-verify a{
    text-decoration: none;
    margin-left: 60px;
    line-height: 24px;;

}


    </style>
</head>
<body>

<div id="main-wrapper">
    <div id="header"style="box-shadow: 0 1px 2px 0 rgba(0,0,0,.25)">
        <div >
                <?php include "./part/header.php" ?>
        </div>
    </div>
    <!-- <div style="width: 100%;height: 1px;border:0.88px solid rgba(0,0,0,.1);"></div> -->
    <div class="bg-register" style="width: 100%;">
    <div class="flex" style="width:100%;height: 800px; padding-top: 22px; padding-left: 122px;">
            <div class="item-6 ">
                <div class="flex" style="background-color:rgba(255, 255, 255, 0.997);border-radius: 4px;box-shadow:1px 2px 3px 3px rgba(0,0,0,.1);">
                    <div class="item-6">
                        <div class="account-profile">
                            <img src="../image/sovann.jpg" alt="">
                            <span>Vin Sovann</span>
                        </div>
                    </div>
                    <div class="item-6">
                            <div class="account-profile">
                                    <img src="../image/profile.jpg" alt="">
                                    <span>Heang Vanna</span>
                                </div>
                    </div>
                    <div class="item-12 ">
                        <div class="other-account">
                            <img src="../image/defo.png" alt="" >
                            <span><a href="">Other Account</a></span>
                            <button class="btn-submit" type="submit">Log In</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item-6" >
                    <div class="login-frm-f-r" id="frm-login-f-r">
                            <form action="" method="POST">
                                <div class="frm-header-r">
                                    <span>Creat Account</span>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" type="text" placeholder="Username" name="username"></div> 
                                <div class="form-group">
                                <input class="form-control" type="email" placeholder="Email" name="email"></div> 
                                    <div class="form-group">
                                    <input class="form-control" type="password" placeholder="Password" name="password"></div> 
                                            <div class="form-group">
                                                    <input class="form-control" type="password" placeholder="Re-password" name="re-password"></div> 
                    
                    
                                <button class="btn-submit" type="button"  onclick="show()">Register</button>
                               
                              
                            </form>
        </div>
            </div>
            
            <div id="pop-up">
            <div id="close-btn" onclick="closePop()"></div>
                <div style="width:72%;height: 388px; margin:0 auto;background-color: #F9F9FA">
                    <h2>More Information</h2><br>
                    <img src="../image/profile.jpg" alt=""><br>
                    <button onclick="showVerify()">Add Photo</button><br>
                    <textarea placeholder="Experience"></textarea>
                </div>
            <script>
        function show(){
            document.getElementById('pop-up').style.display = 'block';
        }
        function closePop(){
            document.getElementById('pop-up').style.display = 'none';
        }
        var x=0;
        if(x==0){
            x = 1;
        }else if(x==1) {
            x = 0;
        }

    </script>
    
    </div>
        </div>
        <div id="pop-up-verify">
            <div id="close-btn" onclick="closePopVerify()"></div>
                <div style="width:72%;height: 388px; margin:0 auto;background-color: #F9F9FA">
                    <img src="../image/profile.jpg" alt=""><br>
                    <p>Code 6 digit</p>
                    <input type="text"  placeholder ="# # # # # #">
                    <p>Please link:</p>
                    <a href="">sansinath@gmail.com</a>
                    <button type="submit">Submit</button><br>
                  
                </div></div>
                </div>
                <script>
        function showVerify(){
            document.getElementById('pop-up-verify').style.display = 'block';
            document.getElementById('pop-up').style.display = 'none';
        }
        function closePopVerify(){
            document.getElementById('pop-up-verify').style.display = 'none';
        }
        var x=0;
        if(x==0){
            x = 1;
        }else if(x==1) {
            x = 0;
        }

    </script>
    </div>
    <footer>
        <?php include "./part/footer.php"?>
    </footer>
</div>
</body>
</html>