<?php include "./part/head.php" ?>
<title>Detail</title>
<style>
    .logo:hover .next,
    .pre {
        visibility: visible;
        cursor: pointer;
    }

    .logo:hover .pre {
        visibility: visible;
        cursor: pointer;
    }

    .next {
        visibility: hidden;
        position: absolute;
        top: 50%;
        right: 0;
        width: 26px;
        height: 22px;
        border-radius: 3px;
        background-color: #fff;
    }

    .pre {
        visibility: hidden;
        position: absolute;
        left: 0px;
        top: 50%;
        width: 26px;
        height: 22px;
        border-radius: 3px;
        background-color: #fff;

    }

    .pre span {
        display: block;
        width: 10px;
        height: 2px;
        background-color: #999;
        margin: 4px 0;
        cursor: pointer;

    }

    .next span {

        display: block;
        width: 10px;
        height: 2px;
        background-color: #999;
        margin: 4px 0;
        /* transition: .5s; */
        cursor: pointer;

    }

    .next span:nth-child(1) {
        transform: rotate(42deg);
    }

    .next span:nth-child(2) {
        transform: rotate(-44deg);

    }

    .pre span:nth-child(1) {
        transform: rotate(-44deg);
    }

    .pre span:nth-child(2) {
        transform: rotate(42deg);

    }


    .slide-box {




        display: none;
    }

    .logo {
        position: relative;width:100%;height: 600px;clear: both;
    }

    .logo .slide-box img{
        width:100%;height: 600px;
    }
    .item-detail-1 {
        flex: 8%;
        max-width: 8%;
    }
    .item-detail-1 img {
        width:66%;
        border-radius: 50%;
        /* position: absolute;
        top: 22%;
        left: 0; */
        box-sizing: border-box;
        margin-top: 11px;
    }
    .item-detail-2 {
        flex: 82%;
        max-width: 80%;
        padding-top: 17px;
    }
    .item-detail-3 {
        flex: 10%;
        max-width: 10%;
    }
    .item-detail-3 {
        padding-top: 32px;
    }
    .detail-ul-li ul{
        padding: 0;
        margin-top: 28px;
        margin-left: 28px;
        list-style-type: none;
        
    } 
    .detail-ul-li ul li a{
            text-decoration: none;
            color: #1d2129;  
            padding: 8px;
            display: block;
            
    }
    .category-nav-u h2 {
        display: block;
        padding: 33px;
        color: #1d2129;
        
    }
</style>
</head>

<body>
    <div id="main-wrapper detail">
        <div id="header">
            <?php include "./part/header.php"; ?>
        </div>
        <!-- nav -->
        <?php include "./part/nav.php" ?>

        <div class="main-content">
            <div class="user-detail-info" style="margin: 0 auto; width:80%;">
                <div class="category-nav-u">
                   <h2>Template</h2>
                </div>
                <div >
                    <div class="logo">
                        <div class="pre">
                            <span></span>
                            <span></span>
                        </div>
                        <div class="slide-box">
                            <img src="../image/d9.jpeg" width="100%" height="60%;" alt="">
                        </div>
                        <div class="slide-box">
                            <img src="../image/c11.jpeg" width="100%" height="60%;" alt="">
                        </div>
                        <div class="slide-box">
                            <img src="../image/c10.jpeg" width="100%" height="60%;" alt="">
                        </div>
                        <div class="slide-box">
                            <img src="../image/d13.jpeg" width="100%" height="60%;" alt="">
                        </div>
                        <div class="slide-box">
                            <img src="../image/d14.jpeg" width="100%" height="60%;" alt="">
                        </div>
                        <div class="next">
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <script>
                        $(document).ready(function () {

                        });
                        var slide = $('.slide-box');
                        var index = 0;
                        var slideBar = slide.length;
                        slide.eq(index).show();
                        $('.next').click(function () {
                            index++;
                            slide.hide();
                            slide.eq(index).show();
                            if (index > slideBar - 1) {
                                index = 0;
                                slide.eq(index).show();
                            }
                        });
                        $('.pre').click(function () {
                            index--;
                            slide.hide();
                            slide.eq(index).show();
                            if (index < 0) {
                                slide.hide();
                                index = slideBar - 1;
                                slide.eq(index).show();
                            }
                        });

                        var mySlide;
                        mySlide = setInterval(alertFunc, 3000);
                        function alertFunc() {
                            index++;
                            slide.hide();
                            slide.eq(index).show();
                            if (index > slideBar - 1) {
                                index = 0;
                                slide.eq(index).show();
                            }
                        }
                        $('.logo').mouseover(function () {
                            clearInterval(mySlide);
                        })

                    </script>

                    <div style="height: 82px;width:100%;margin: 0;; background-color: #fff;    border: 1px solid rgba(100, 100, 100, .4);
                    border-radius: 0 0 2px 2px;
                    box-shadow: 2px 3px 8px rgba(0, 0, 0, .25);
                    color: #1d2129;   display: block;padding-left: 22px;position: relative;display: flex;flex-direction:row;">
                            <div class="item-detail-1">
                                <img src="../image/vanna.jpg" alt="" >
                            </div>
                            <div  class="item-detail-2">
                                <h4 style="display: inline;">Banner Coffee</h4>
                                <div class="u-title" style="padding-top:12px;"><span> Date post:22-Dec-218 </span><span> Locations : Phnom Penh </span>
                                    <span>Views: 22k</span>
                                </div>
                            </div>
                            <div  class="item-detail-3">
                                share
                            </div>
                    </div>

                    <div class="flex">
                        <div class="item-8 detail-ul-li">
                            <ul>
                                <li><a href="">Price</a></li>
                                <li><a href="">Email</a></li>
                                <li><a href="">Website</a></li>
                            </ul>
                        </div>
                        <div class="item-4" >
                            <div style="width:77%;height: auto;margin: 0 auto;padding: 22px;background-color:#d4d9dd17; margin-top:33px;margin-bottom: 33px; box-sizing: border-box; display: block; border: 1px solid #e5e5e5;border-radius: 3px;">
                                <h3 style="text-align:center;color: #1d2129;  ">More Design</h3>
                                <p>Logo Similar ?</p>
                                <p>Banner logo ?</p>
                                <p>Graphic Design ?</p>
                                <p>Edit Photo ?</p>
                                <p>Web Design ?</p>
                                <p>Vidio </p>
                            </div>
                        </div>
                        <img src="../image/ad.jpg" alt="" width="100%" height="16%">
                        <div style="padding:22px;margin-top: 33px;" class="border">
                            <h3>Descriptions</h3>
                            <p>ខ្ញុំបាទទទួលរចនា Design Logo គ្រប់ប្រភេទសំរាប់ក្រុមហ៊ុន, សហគ្រាសនឹងស្ថាប័នផ្សេងៗ...ជា
                                soft
                                file&#40;Ai file, PNG, EPS, and PDF&#41; ដោយតំលៃធូរថ្លៃជាងទីផ្សារ។
                                ដូច្នេះបេីបងប្អូនឬម្ចាស់ក្រុមហ៊ុន សហគ្រាសឬស្ថាប័នណាត្រូវការសេវាកម្មរចនា Logo
                                សូមកុំភ្លេចទាក់ទងខ្ញុំបាទ ។
                            </p>
                        </div>
                        <button style=" padding: 7px 18px;margin: 44px 450px 44px;
                        border: none;
                        border-radius: 4px;
                        box-shadow: 0;
                        background-color: #0EB48C;
                        color: #f1f1f1;
                        
                        outline: 0;
                        cursor: pointer;">Contact</button>
                    </div>
                </div>
                <h2 style="color:#1d2129;padding-left: 28px;">Relative</h2>
                <article>
                    <div class="box-item ">
                        <div class="flex">
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/c.jpeg" alt="" class="item-ad-img">
                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>
                                        <h3 class="item-descript">I will design responsive and
                                            prfessional
                                            websites. I will create web app development ...</h3>
                                        <div class="item-love">
                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="2"
                                                height="2">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">
                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">
                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                        </div>
                                    </div>
                                    <footer class="item-footer">
                                        <div class="price">
                                            <span>
                                                <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                    height="15">
                                                    <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                </svg>
                                                <span>
                                                    <strong>5.0</strong>
                                                    (62)
                                                </span>
                                            </span>
                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/b8.jpeg" alt="" class="item-ad-img">
                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>
                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>
                                        <div class="item-love">
                                            <svg id="eye" viewBox="0 0 30.5 16.5" width="16" height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">
                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                            <svg id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">
                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                        </div>
                                    </div>
                                    <footer class="item-footer">
                                        <div class="price">
                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>

                                        </div>
                                    </footer>
                                </div>
                            </div>
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/d3.jpg" alt="" class="item-ad-img">

                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>

                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>


                                        <div class="item-love">

                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                </path>

                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                        </div>
                                    </div>

                                    <footer class="item-footer">
                                        <div class="price">

                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>

                                        </div>
                                    </footer>

                                </div>
                            </div>
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/c5.jpeg" alt="" class="item-ad-img">

                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>

                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>


                                        <div class="item-love">

                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                </path>

                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                        </div>
                                    </div>

                                    <footer class="item-footer">
                                        <div class="price">

                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>


                                        </div>
                                    </footer>

                                </div>
                            </div>
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/d2 - Copy.jpg" alt="" class="item-ad-img">

                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>



                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>


                                        <div class="item-love">

                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                </path>

                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                        </div>
                                    </div>


                                    <footer class="item-footer">
                                        <div class="price">


                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>

                                        </div>
                                    </footer>

                                </div>
                            </div>
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/d10.jpeg" alt="" class="item-ad-img">
                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>
                                        <h3 class="item-descript">I will design responsive and
                                            prfessional
                                            websites. I will create web app development ...</h3>
                                        <div class="item-love">

                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                </path>

                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>


                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>


                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>


                                        </div>
                                    </div>


                                    <footer class="item-footer">
                                        <div class="price">
                                            <span>
                                                <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                    height="15">
                                                    <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                </svg>
                                                <span>
                                                    <strong>5.0</strong>
                                                    (62)
                                                </span>
                                            </span>
                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>


                                        </div>
                                    </footer>

                                </div>
                            </div>
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/d11.jpeg" alt="" class="item-ad-img">

                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>

                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>


                                        <div class="item-love">

                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                </path>

                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>


                                        </div>
                                    </div>

                                    <footer class="item-footer">
                                        <div class="price">


                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>

                                        </div>
                                    </footer>

                                </div>
                            </div>
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/d12.jpeg" alt="" class="item-ad-img">

                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>

                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>


                                        <div class="item-love">

                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                </path>

                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                        </div>
                                    </div>

                                    <footer class="item-footer">
                                        <div class="price">

                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>

                                        </div>
                                    </footer>

                                </div>
                            </div>
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/d13.jpeg" alt="" class="item-ad-img">

                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>

                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>


                                        <div class="item-love">

                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                </path>

                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                        </div>
                                    </div>

                                    <footer class="item-footer">
                                        <div class="price">

                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>


                                        </div>
                                    </footer>

                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
        </div>

        <footer>
            <?php include "./part/footer.php" ?>
        </footer>

        <?php include "./part/go-top.php" ?>

    </div>
</body>
</html>