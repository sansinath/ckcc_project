<?php ?>
<?php include "./part/head.php" ?>

<title>Home page</title>
<style>
    .flex img {
        border-radius: 8px;
    }

    .flex .pictures {
        padding: 0 8px;
        box-sizing: border-box;
    }

    .flex:after,
    .flex:before {
        content: "";
        display: table;
        clear: both
    }

    .pictures {
        float: left;
        width: 100%
    }


    @media (min-width:601px) {
        .pictures {
            /* width: 33.33333% */
            width: 24.99999%
        }
    }
    .item a {
    text-decoration: none;
}
}
    .home-box-slide {
        /* display:none; */
    }

    /*---------------login ---------*/
    

</style>
</head>

<body>
<div id="main-wrapper">
        <?php   
        // include "./part/login.php"; 
        ?>
         <div class="login-frm-f" id="frm-login-f">
                    <form action="">
                        <div class="frm-header">
                            
                            <span>  Log In to </span>
                            
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="email" placeholder="Email / Username" name="username"></div> 
                        <div class="form-group">
                            <input class="form-control" type="password" placeholder="Password" name="password"></div> 
                        <div class="under-solid">
                            <span class="solid-1"></span>
                            <span class="solid-or ">or</span>
                            <span class="solid-2" ></span>
                        </div>
            
                        <div style="background-color: #4267B2;border-radius: 5px;" >
                            <button class="form-control-connect " type="button">
                                <a href="">
                                    <img src="../image/download.png" alt="" width="10%;">
                                    <span>
                                        Continue as facebook
                                    </span> 
                                </a>
                            </button>
                        </div>
                        <div style="background-color:#FAFAFA;color: #FAFAFA;border-radius: 5px;">
                            <button class="form-control-connect" type="button">
                                <a href="">
                                    <img src="../image/download.jpg" alt="" width="10%;">
                                    <span style="color: #555">
                                    Continue as google
                                    </span>    
                                </a>
                            </button>
                        </div>
            
                        <button class="btn-submit" type="submit">Log In</button>
                        <span style="color: #555;font-size: 16px;">
                            <input type="checkbox" > 
                            Remember me ?
                        </span>
                        <a href="#" class="forgot">
                            Forgot your email or password?
                        </a> 
                    </form>
</div>
<script>
    // $(document).ready(function(){
    //     $('#frm-login-f').hide();
    //     $('#btn-frm-login-f').click(function(){
    //         $('#frm-login-f').show();
            
    //         $("body").blur(function(){
    //             $('.main-wrapper').css("background","red");
    //         })

    //     })
    // })
</script>
<!-- // -------------------- -->
    <div style="width:100%;margin:0 auto; position: relative;">
        <div id="header">
            <?php include "./part/header.php" ?>
        </div>

        <img src="../image/c1.jpeg" width="100%" alt="">
    </div>

    <section>
        <div class="home-profile-view-header">
            <h2>Most Profile Viewed</h2>
        </div>
        <div class="container">
            <div class="pre">
                <span></span>
                <span></span>
            </div>

            <div style="width:92%;margin:0 auto; ">
                <article>
                    <div class="box-item ">
                        <div class="flex">
                            <div class="home-box-slide item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <a href="../view/mainPage.php">
                                        <img src="../image/c.jpeg" alt="" class="item-ad-img">
                                        <div class="item-profile">
                                            <div class="photo-user">
                                                <span class="user-img">
                                                    <img src="../image/vanna.jpg">
                                                </span>
                                                <span class="username">
                                                    Heang Vanna
                                                </span>
                                            </div>
                                            <h3 class="item-descript">I will design responsive and
                                                prfessional
                                                websites. I will create web app development ...</h3>
                                            <div class="item-love">
                                                <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="2"
                                                    height="2">
                                                    <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">
                                                    </path>
                                                </svg>
                                                <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                                <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8"
                                                    width="16" height="16">
                                                    <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                                </svg>
                                                <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                                <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16"
                                                    height="16">
                                                    <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">
                                                    </path>
                                                </svg>
                                                <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                            </div>
                                        </div>
                                        <footer class="item-footer">
                                            <div class="price">
                                                <span>
                                                    <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                        height="15">
                                                        <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                    </svg>
                                                    <span>
                                                        <strong>5.0</strong>
                                                        (62)
                                                    </span>
                                                </span>
                                                <small style="color:#34A853">
                                                    Starting At
                                                </small>

                                                <small style="color:#FA3E3E"> $222</small>
                                            </div>
                                        </footer>
                                    </a>
                                </div>
                            </div>
                            <div class="home-box-slide item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/b8.jpeg" alt="" class="item-ad-img">
                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>
                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>
                                        <div class="item-love">
                                            <svg id="eye" viewBox="0 0 30.5 16.5" width="16" height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">
                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                            <svg id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">
                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                        </div>
                                    </div>
                                    <footer class="item-footer">
                                        <div class="price">
                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>

                                        </div>
                                    </footer>
                                </div>
                            </div>
                            <div class="home-box-slide item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/d3.jpg" alt="" class="item-ad-img">

                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>

                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>


                                        <div class="item-love">

                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                </path>

                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                        </div>
                                    </div>

                                    <footer class="item-footer">
                                        <div class="price">

                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>

                                        </div>
                                    </footer>

                                </div>
                            </div>
                            <div class="home-box-slide item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/c5.jpeg" alt="" class="item-ad-img">

                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>

                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>


                                        <div class="item-love">

                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                </path>

                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                        </div>
                                    </div>

                                    <footer class="item-footer">
                                        <div class="price">

                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>


                                        </div>
                                    </footer>

                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>

            <div class="next">
                <span></span>
                <span></span>
            </div>
        </div>

        <div class="home-profile-rated-header">
            <h2>Most Profile Rated</h2>
        </div>
        <div class="container">
            <div class="pre">
                <span></span>
                <span></span>
            </div>
            <div style="width:92%;margin:0 auto; ">
                <article>
                    <div class="box-item ">
                        <div class="flex">
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/c.jpeg" alt="" class="item-ad-img">
                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>
                                        <h3 class="item-descript">I will design responsive and
                                            prfessional
                                            websites. I will create web app development ...</h3>
                                        <div class="item-love">
                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="2"
                                                height="2">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">
                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">
                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong></strong></span>
                                        </div>
                                    </div>
                                    <footer class="item-footer">
                                        <div class="price">
                                            <span>
                                                <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                    height="15">
                                                    <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                                </svg>
                                                <span>
                                                    <strong>5.0</strong>
                                                    (62)
                                                </span>
                                            </span>
                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/b8.jpeg" alt="" class="item-ad-img">
                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>
                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>
                                        <div class="item-love">
                                            <svg id="eye" viewBox="0 0 30.5 16.5" width="16" height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">
                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                            <svg id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">
                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>
                                        </div>
                                    </div>
                                    <footer class="item-footer">
                                        <div class="price">
                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>

                                        </div>
                                    </footer>
                                </div>
                            </div>
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/d3.jpg" alt="" class="item-ad-img">

                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>

                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>


                                        <div class="item-love">

                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                </path>

                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                        </div>
                                    </div>

                                    <footer class="item-footer">
                                        <div class="price">

                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>

                                        </div>
                                    </footer>

                                </div>
                            </div>
                            <div class="item-6 item-sm-6 item-md-4 item-lg-3 item-xl-3">
                                <div class="item">
                                    <img src="../image/c5.jpeg" alt="" class="item-ad-img">

                                    <div class="item-profile">
                                        <div class="photo-user">
                                            <span class="user-img">
                                                <img src="../image/vanna.jpg">
                                            </span>
                                            <span class="username">
                                                Heang Vanna
                                            </span>
                                        </div>

                                        <h3 class="item-descript">I will design responsive and
                                            prfessional websites. I will create web app development ...</h3>


                                        <div class="item-love">

                                            <svg style="fill: #a8a6b7;" id="eye" viewBox="0 0 30.5 16.5" width="16"
                                                height="16">
                                                <path d="M15.3 0C8.9 0 3.3 3.3 0 8.3c3.3 5 8.9 8.3 15.3 8.3s12-3.3 15.3-8.3C27.3 3.3 21.7 0 15.3 0zm0 14.5c-3.4 0-6.2-2.8-6.2-6.2C9 4.8 11.8 2 15.3 2c3.4 0 6.2 2.8 6.2 6.2 0 3.5-2.8 6.3-6.2 6.3z">\

                                                </path>

                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="comment" viewBox="-405.9 238 56.3 54.8" width="16"
                                                height="16">
                                                <path d="M-391 291.4c0 1.5 1.2 1.7 1.9 1.2 1.8-1.6 15.9-14.6 15.9-14.6h19.3c3.8 0 4.4-.8 4.4-4.5v-31.1c0-3.7-.8-4.5-4.4-4.5h-47.4c-3.6 0-4.4.9-4.4 4.5v31.1c0 3.7.7 4.4 4.4 4.4h10.4v13.5z"></path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                            <svg style="fill:#a8a6b7" id="heart" viewBox="0 0 100 92" width="16" height="16">
                                                <path d="M85.24 2.67C72.29-3.08 55.75 2.67 50 14.9 44.25 2 27-3.8 14.76 2.67 1.1 9.14-5.37 25 5.42 44.38 13.33 58 27 68.11 50 86.81 73.73 68.11 87.39 58 94.58 44.38c10.79-18.7 4.32-35.24-9.34-41.71z">

                                                </path>
                                            </svg>
                                            <span style="color:#a6a6b0;font-size: 15px;padding-left:1px;"><strong>22k</strong></span>

                                        </div>
                                    </div>

                                    <footer class="item-footer">
                                        <div class="price">

                                            <svg style="fill:#4CAF50" class="" viewBox="0 0 1792 1792" width="15"
                                                height="15">
                                                <path d="M1728 647q0 22-26 48l-363 354 86 500q1 7 1 20 0 21-10.5 35.5t-30.5 14.5q-19 0-40-12l-449-236-449 236q-22 12-40 12-21 0-31.5-14.5t-10.5-35.5q0-6 2-20l86-500-364-354q-25-27-25-48 0-37 56-46l502-73 225-455q19-41 49-41t49 41l225 455 502 73q56 9 56 46z"></path>
                                            </svg>
                                            <span>
                                                <strong>5.0</strong>
                                                (62)
                                            </span>

                                            <small style="color:#34A853">
                                                Starting At
                                            </small>

                                            <small style="color:#FA3E3E"> $222</small>


                                        </div>
                                    </footer>

                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="next">
                <span></span>
                <span></span>
            </div>
        </div>

        <div style="width:100%; background-color: #85A7A8; margin-top: 42px;">
            <img src="../image/quote2.jpg" width="30%" height="227px;" style="margin-left: 32%" alt="">
        </div>

        <div style="background-color: #f2f2f2; ">
            <div class="container-f" style="padding: 77px 0 77px 0;">
                <div class="item-f">
                    <a href="../html/mainPage.html">
                        <img src="../image/pen.png" style="margin-left:35px;" alt="">
                    </a>
                </div>
                <div class="item-f">
                    <a href="../html/mainPage.html">
                        <img src="../image/computer.png" alt="" style="margin-left:8px;">
                    </a>
                </div>
            </div>
            <div style="background-color: #9A9933">
                <img src="../image/quote1.jpg" width="30%" height="227px;" style="margin-left: 32%" alt="">
            </div>
            <div class="container">
                <div class="flex" style="width: 100%; height: 300px; margin:128px 0 22px 0;background-color: #f2f2f2;">
                    <div class="pictures">
                        <a href="../html/mainPage.html">
                            <img src="../image/c.jpeg" style="width:100%">
                        </a>
                    </div>
                    <div class="pictures">
                        <img src="../image/web.jpeg" style="width:100%">
                    </div>
                    <div class="pictures">
                        <img src="../image/d3.jpg" style="width:100%">
                    </div>
                    <div class="pictures">
                        <img src="../image/d11.jpeg" style="width:100%">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <?php include "./part/footer.php"?>
    </footer>
    </div>

    <script>
        $(document).ready(function () {

        });
        var slide = $('.home-box-slide');

        var index = 0;
        var slideBar = slide.length;
        // for($i=0;$i<4;$i++)
        //     slide.eq($i).show();
        $('.next').click(function () {
            slide.hide();
            for ($i = index; $i < 4; $i++)
                slide.eq($i).show();
            index++;

            // slide.eq(index).show();


            if (index > slideBar - 1) {
                index = 0;
                slide.eq(index).show();
            }
        });
        $('.pre').click(function () {
            index--;
            slide.hide();
            slide.eq(index).show();
            if (index < 0) {
                slide.hide();
                index = slideBar - 1;
                slide.eq(index).show();
            }
        });

        // var mySlide ;
        // mySlide = setInterval(alertFunc, 3000);

        // function alertFunc(){
        //     index++;
        //     slide.hide();
        //     slide.eq(index).show();
        //     if(index >slideBar-1) {
        //         index=0;
        //         slide.eq(index).show();
        //     }
        // }
        // $('.logo').mouseover(function(){
        //    clearInterval(mySlide);
        // })
    </script>
</div>
</body>
</html>