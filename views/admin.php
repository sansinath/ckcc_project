<?php include "./part/head.php" ?>
<title>Document</title>
<style>
    .admin-accuont-user {
        position: relative;
        box-sizing: border-box;
        display: flex;
        color: #a6a6b0;
        /* background-color: #F4F3F4; */
        border-radius:2px;
        border-bottom: 1px solid #e5e5e5;
        width: 100%;
        padding-bottom: 12px;
        margin: 0;
        padding: 0;
        border: 1px solid #F4F3F4;
    }

    .admin-accuont-user .accuont-user-td-1 {
        flex: 20%;
        padding: 0;
        margin: 0;
        height: 77px;
        position: relative;
    }

    .admin-accuont-user .accuont-user-td-2 {
        flex: 15%;
        padding: 22px;
    }

    .admin-accuont-user .accuont-user-td-3 {
        flex: 25%;
        padding: 22px;
    }

    .admin-accuont-user .accuont-user-td-4 {
        flex: 30%;
        padding: 22px;

    }

    .admin-accuont-user .accuont-user-td-5 {
        box-sizing: border-box;
        flex: 10%;
        padding: 22px;

        position: relative;
    }

    .admin-accuont-user .accuont-user-td-1 img {
        width: 22%;
        /* margin-left:-55px; */
        border-radius: 50%;
        position: absolute;
        top: 16%;
        left:6%;

    }

    .admin-accuont-user .accuont-user-td-1 span {
        position: absolute;
        top: 26%;
        left: 32%;


    }

    .admin-accuont-user .accuont-user-td-1 p {
        position: absolute;
        top: 44%;
        left: 32%;
        font-size: 12px;
    }

    .admin-accuont-user .accuont-user-td-3 span {
        /* margin-left:17px; */

        margin-left: 18px;
    }


    .admin-accuont-user button {
        margin: 0px;
        padding: 6px 11px;
        border: none;
        border-radius: 6px;
        box-shadow: 0;
        background-color: rgb(253, 108, 108);
        color: #555;
        text-shadow: none;
        outline: 0;
        cursor: pointer;

    }
    #aside {
        margin: 0;
        padding: 0;
        width: 100%;
    }
    #aside .asidebar{
       border: 1px solid;
    }
    #aside form {
        padding-top: 22px;
        padding-left: 22px;
        padding-right: 22px;
        width: 100%;
        height: 444px;
        background-color: #FAFAFA;
    }
 
    #aside .styled-select {
    border: 1px solid #e5e5e5;
    border-radius: 6px;
    outline: none;
    background: url('../image/drop.png') #fff; 
    background-size: 18px;
    text-indent: 10px;
    z-index: 66;
    background-position: 196px 8px;
   background-repeat: no-repeat;
    }
    #aside .in{
    border: 1px solid #e5e5e5;
    border-radius: 6px;
    outline: none;
    background: url('../image/drop.png') #fff; 
    background-size: 18px;
    text-indent: 10px;
    z-index: 66;
    background-position: 262px 8px;
   background-repeat: no-repeat;
    }


    #aside input {
 
    height: 33px;
     width: 100%;
    box-sizing: border-box;
  padding-right: 7px;
    border: 1px solid #e5e5e5;
    border-radius: 6px;

    outline: none;
    }
    #aside select {
    height: 33px;
     width: 100%;
    box-sizing: border-box;
  padding-right: 7px;
    border: none;
    -webkit-appearance:button;
    outline: none;
    margin-top: 22px;
    
   

    }

    #aside .styled-select option.service-small {
    font-size: 14px;
    padding: 5px;
    background: #fff;
    color: #777;
    border-radius: 6px;
    border: 1px solid #e5e5e5;
    outline: none;
    }

#aside button {
    margin-top: 44px;
        margin-left:82px;
            padding: 6px 11px;
        border: 1px solid #a6a6b0;
        border-radius: 6px;
        box-shadow: 0;
        background-color: #fff;
        color: #555;
        text-shadow: none;
        outline: 0;
        cursor: pointer;

    }
  .all-post {
      
        padding: 6px 11px;
        border: 1px solid #a6a6b0;
        border-radius: 6px;
        box-shadow: 0;
        background-color: #7E96B9;
        color: #555;
        text-shadow: none;
        outline: 0;
        color:#fff;
        cursor: pointer;
    }
   .all-post span{
        color:#fff;
        padding-left:22px;

     }
      .active-user {
       
       padding: 6px 11px;
       border: 1px solid #a6a6b0;
       border-radius: 6px;
       box-shadow: 0;
       background-color: #6FBDDC;
       color: #555;
       text-shadow: none;
       outline: 0;
       color:#fff;
       cursor: pointer;
   }
    .active-user span{
        color:#fff;
        padding-left:12px;

     }
    #aside .sidebar ul {
        margin:22px 0 22px;
    }
    #aside .sidebar ul li {
        padding-left:37px;
        
    }
    #aside .sidebar ul li a{
        display:block;
        padding:7px;
    }
    #aside .sidebar .menu {
            display:block;
            position:absolute;
            top:22px;
            left:12px;
            width:60px;
            height:50px;
            background-color: #ABDCE7; ;
            cursor:pointer;
            z-index:1;
            box-sizing:border-box;
        }
        #aside .sidebar .menu span {
            margin-top: 22px;
            position:absolute;
            width:30px;
            height: 3px;
            background-color: #262626;
            display: block;
            top: calc(50% -1px);
            left: calc(50% - 15px);
            transform: 0.5s;

        }
        #aside .sidebar .menu span:nth-child(1){
            transform:translateY(-10px);
        }
        #aside .sidebar .menu span:nth-child(3){
            transform:translateY(10px);
        }
        #aside .sidebar .menu.active span:nth-child(1){
            transform: translateY(0px) rotate(-45deg);
            
        }
        #aside .sidebar .menu.active span:nth-child(3){
            transform: translateY(0px) rotate(45deg);
            
        }
        #aside .sidebar .menu.active span:nth-child(2){
            transform: translateY(0px) rotate(-30deg);
            opacity: 0;
            
        }
</style>
</head>

<body>
    <div id="main-wrapper">
        <div id="header">
            <header class="base-header">
                <div class="header-row">
                    <div class="header-cols">
                        <div class="head-col-1 ">
                            <img src="../image/logo.jpg" width="100%" alt="">
                        </div>
                        <div class="head-col-2">

                        </div>
                        <div class="head-col-3">

                        </div>
                        <div class="head-col-4"></div>

                        <div class="head-col-6" style="margin-right:22px;">
                            <div class="all-post">

                                <span> All Post</span>

                            </div>
                        </div>
                        <div class="head-col-7">
                            <div class="active-user">
                                <span> Active User</span>

                            </div>
                        </div>
                    </div>
                </div>
            </header>
        </div>

        <div class="main-content" style="margin:0;">

            <section>
                <div class="flex">
                    <div class="item-3" style="padding:0">
                        <aside id="aside">
                            <div class="sidebar ">
                                <div style="height: 88px; background-color: #ABDCE7; position:relative;">
                                    <script>
                                        $(document).ready(function(){
            $('.menu').click(function(){
                    $('.menu').toggleClass('active')
                  
            })
        })
    </script>
                                    <div class="menu">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                    <h2 style="text-align:center;padding-top:32px;">Name Web</h2>
                                </div>
                                <ul>
                                    <li><a href="">Home</a></li>
                                    <li><a href="">Post Today</a></li>
                                    <li><a href="">View Manager</a></li>
                                    <li><a href="">Month Post</a></li>
                                    <li><a href="">View Page</a></li>
                                </ul>
                            </div>
                            <div>
                                <form action="">
                                    <input type="text" placeholder="Username" class="in">
                                    <select class="styled-select">
                                        <option class="service-small"></option>
                                        <option class="service-small"></option>
                                        <option class="service-small"></option>
                                    </select>
                                    <select class="styled-select">
                                        <option class="service-small"></option>
                                        <option class="service-small"></option>
                                        <option class="service-small"></option>
                                    </select><br>
                                    <select class="styled-select">
                                        <option class="service-small"></option>
                                        <option class="service-small"></option>
                                        <option class="service-small"></option>
                                    </select><br>
                                    <select class="styled-select">
                                        <option class="service-small"></option>
                                        <option class="service-small"></option>
                                        <option class="service-small"></option>
                                    </select><br>
                                    <select class="styled-select">
                                        <option class="service-small"></option>
                                        <option class="service-small"></option>
                                        <option class="service-small"></option>
                                    </select>
                                    <button>Search</button>
                                </form>
                                <div style="width:97%; margin:0 auto;border: 1.88px solid #e5e5e5; padding: 8px;margin-top: 22px;">
                                <h2 style="text-align:center;color:#777;font-size:18px;">User's Recently Post</h2>
                                <div class="admin-accuont-user" style="background-color:#E7E7E7;height: 70px;margin-top:8px;">
                                    <div class="accuont-user-td-1">
                                        <img src="../image/profile.jpg" alt="" width="7%">
                                        <span style="font-size:12px;">Heang Vanna</span>
                                        <p style="font-size:12px;">heangvanna@gmail.com</p>
                                    </div>
                                    <div class="accuont-user-td-4" style=" padding-top: 22px;padding-left: 18px;padding-bottom: 0px;padding-right: 0px;font-size: 9px;">11:32:00PM
                                        22 December 2018</div>
                                </div>
                                <div class="admin-accuont-user" style="background-color:#E7E7E7;height: 70px;margin-top:8px;">
                                    <div class="accuont-user-td-1">
                                        <img src="../image/profile.jpg" alt="" width="7%">
                                        <span style="font-size:12px;">Heang Vanna</span>
                                        <p style="font-size:12px;">heangvanna@gmail.com</p>
                                    </div>
                                    <div class="accuont-user-td-4" style="padding-top: 22px;padding-left: 18px;padding-bottom: 0px;padding-right: 0px;font-size: 9px;">11:32:00PM
                                        22 December 2018</div>
                                </div>
                                <div class="admin-accuont-user" style="background-color:#E7E7E7;height: 70px;margin-top:8px;">
                                    <div class="accuont-user-td-1">
                                        <img src="../image/profile.jpg" alt="" width="7%">
                                        <span style="font-size:12px;">Heang Vanna</span>
                                        <p style="font-size:12px;">heangvanna@gmail.com</p>
                                    </div>
                                    <div class="accuont-user-td-4" style="padding-top: 22px;padding-left: 18px;padding-bottom: 0px;padding-right: 0px;font-size: 9px;">11:32:00PM
                                        22 December 2018</div>
                                </div>
                                <div class="admin-accuont-user" style="background-color:#E7E7E7;height: 70px;margin-top:8px;">
                                    <div class="accuont-user-td-1">
                                        <img src="../image/profile.jpg" alt="" width="7%">
                                        <span style="font-size:12px;">Heang Vanna</span>
                                        <p style="font-size:12px;">heangvanna@gmail.com</p>
                                    </div>
                                    <div class="accuont-user-td-4" style="padding-top: 22px;padding-left: 18px;padding-bottom: 0px;padding-right: 0px;font-size: 9px;">11:32:00PM
                                        22 December 2018</div>
                                </div>
                                <div class="admin-accuont-user" style="background-color:#E7E7E7;height: 70px;margin-top:8px;">
                                    <div class="accuont-user-td-1">
                                        <img src="../image/profile.jpg" alt="" width="7%">
                                        <span style="font-size:12px;">Heang Vanna</span>
                                        <p style="font-size:12px;">heangvanna@gmail.com</p>
                                    </div>
                                    <div class="accuont-user-td-4" style="padding-top: 22px;padding-left: 18px;padding-bottom: 0px;padding-right: 0px;font-size: 9px;">11:32:00PM
                                        22 December 2018</div>
                                </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                    <div class="item-9">
                        <div style="height: 88px;background-color: #F1F0F0;">
                            <h2 style="padding-top:32px;padding-left:28px;">All Post</h2>
                        </div>
                        <div class="admin-accuont-user">
                            <div class="accuont-user-td-1">
                                <img src="../image/profile.jpg" alt="" width="7%">
                                <span>Heang Vanna</span>
                                <p>heangvanna@gmail.com</p>
                            </div>

                            <div class="accuont-user-td-2">Logo Design</div>
                            <div class="accuont-user-td-3">
                                <span style="margin-left:2px;">$7</span> <span>View <span style="margin-left:2px;">22k</span></span>
                                <span>Rated <span style="margin-left:2px;">44k</span></span>
                            </div>
                            <div class="accuont-user-td-4">11:32:00PM 22 December 2018</div>
                            <div class="accuont-user-td-5">

                                <button>Delete</button>

                            </div>

                        </div>
                        <div class="admin-accuont-user">
                            <div class="accuont-user-td-1">
                                <img src="../image/profile.jpg" alt="" width="7%">
                                <span>Heang Vanna</span>
                                <p>heangvanna@gmail.com</p>
                            </div>

                            <div class="accuont-user-td-2">Logo Design</div>
                            <div class="accuont-user-td-3">
                                <span style="margin-left:2px;">$7</span> <span>View <span style="margin-left:2px;">22k</span></span>
                                <span>Rated <span style="margin-left:2px;">44k</span></span>
                            </div>
                            <div class="accuont-user-td-4">11:32:00PM 22 December 2018</div>
                            <div class="accuont-user-td-5">

                                <button>Delete</button>

                            </div>

                        </div>
                        <div class="admin-accuont-user">
                            <div class="accuont-user-td-1">
                                <img src="../image/profile.jpg" alt="" width="7%">
                                <span>Heang Vanna</span>
                                <p>heangvanna@gmail.com</p>
                            </div>

                            <div class="accuont-user-td-2">Logo Design</div>
                            <div class="accuont-user-td-3">
                                <span style="margin-left:2px;">$7</span> <span>View <span style="margin-left:2px;">22k</span></span>
                                <span>Rated <span style="margin-left:2px;">44k</span></span>
                            </div>
                            <div class="accuont-user-td-4">11:32:00PM 22 December 2018</div>
                            <div class="accuont-user-td-5">

                                <button>Delete</button>

                            </div>

                        </div>
                        <div class="admin-accuont-user">
                            <div class="accuont-user-td-1">
                                <img src="../image/profile.jpg" alt="" width="7%">
                                <span>Heang Vanna</span>
                                <p>heangvanna@gmail.com</p>
                            </div>

                            <div class="accuont-user-td-2">Logo Design</div>
                            <div class="accuont-user-td-3">
                                <span style="margin-left:2px;">$7</span> <span>View <span style="margin-left:2px;">22k</span></span>
                                <span>Rated <span style="margin-left:2px;">44k</span></span>
                            </div>
                            <div class="accuont-user-td-4">11:32:00PM 22 December 2018</div>
                            <div class="accuont-user-td-5">

                                <button>Delete</button>

                            </div>

                        </div>
                        <div class="admin-accuont-user">
                            <div class="accuont-user-td-1">
                                <img src="../image/profile.jpg" alt="" width="7%">
                                <span>Heang Vanna</span>
                                <p>heangvanna@gmail.com</p>
                            </div>

                            <div class="accuont-user-td-2">Logo Design</div>
                            <div class="accuont-user-td-3">
                                <span style="margin-left:2px;">$7</span> <span>View <span style="margin-left:2px;">22k</span></span>
                                <span>Rated <span style="margin-left:2px;">44k</span></span>
                            </div>
                            <div class="accuont-user-td-4">11:32:00PM 22 December 2018</div>
                            <div class="accuont-user-td-5">

                                <button>Delete</button>

                            </div>

                        </div>
                        <div class="admin-accuont-user">
                            <div class="accuont-user-td-1">
                                <img src="../image/profile.jpg" alt="" width="7%">
                                <span>Heang Vanna</span>
                                <p>heangvanna@gmail.com</p>
                            </div>

                            <div class="accuont-user-td-2">Logo Design</div>
                            <div class="accuont-user-td-3">
                                <span style="margin-left:2px;">$7</span> <span>View <span style="margin-left:2px;">22k</span></span>
                                <span>Rated <span style="margin-left:2px;">44k</span></span>
                            </div>
                            <div class="accuont-user-td-4">11:32:00PM 22 December 2018</div>
                            <div class="accuont-user-td-5">

                                <button>Delete</button>

                            </div>

                        </div>
                        <div class="admin-accuont-user">
                            <div class="accuont-user-td-1">
                                <img src="../image/profile.jpg" alt="" width="7%">
                                <span>Heang Vanna</span>
                                <p>heangvanna@gmail.com</p>
                            </div>

                            <div class="accuont-user-td-2">Logo Design</div>
                            <div class="accuont-user-td-3">
                                <span style="margin-left:2px;">$7</span> <span>View <span style="margin-left:2px;">22k</span></span>
                                <span>Rated <span style="margin-left:2px;">44k</span></span>
                            </div>
                            <div class="accuont-user-td-4">11:32:00PM 22 December 2018</div>
                            <div class="accuont-user-td-5">

                                <button>Delete</button>

                            </div>

                        </div>
                        <div class="admin-accuont-user">
                            <div class="accuont-user-td-1">
                                <img src="../image/profile.jpg" alt="" width="7%">
                                <span>Heang Vanna</span>
                                <p>heangvanna@gmail.com</p>
                            </div>

                            <div class="accuont-user-td-2">Logo Design</div>
                            <div class="accuont-user-td-3">
                                <span style="margin-left:2px;">$7</span> <span>View <span style="margin-left:2px;">22k</span></span>
                                <span>Rated <span style="margin-left:2px;">44k</span></span>
                            </div>
                            <div class="accuont-user-td-4">11:32:00PM 22 December 2018</div>
                            <div class="accuont-user-td-5">

                                <button>Delete</button>

                            </div>

                        </div>
                        <div class="admin-accuont-user">
                            <div class="accuont-user-td-1">
                                <img src="../image/profile.jpg" alt="" width="7%">
                                <span>Heang Vanna</span>
                                <p>heangvanna@gmail.com</p>
                            </div>

                            <div class="accuont-user-td-2">Logo Design</div>
                            <div class="accuont-user-td-3">
                                <span style="margin-left:2px;">$7</span> <span>View <span style="margin-left:2px;">22k</span></span>
                                <span>Rated <span style="margin-left:2px;">44k</span></span>
                            </div>
                            <div class="accuont-user-td-4">11:32:00PM 22 December 2018</div>
                            <div class="accuont-user-td-5">

                                <button>Delete</button>

                            </div>

                        </div>
                        <div class="admin-accuont-user">
                            <div class="accuont-user-td-1">
                                <img src="../image/profile.jpg" alt="" width="7%">
                                <span>Heang Vanna</span>
                                <p>heangvanna@gmail.com</p>
                            </div>

                            <div class="accuont-user-td-2">Logo Design</div>
                            <div class="accuont-user-td-3">
                                <span style="margin-left:2px;">$7</span> <span>View <span style="margin-left:2px;">22k</span></span>
                                <span>Rated <span style="margin-left:2px;">44k</span></span>
                            </div>
                            <div class="accuont-user-td-4">11:32:00PM 22 December 2018</div>
                            <div class="accuont-user-td-5">

                                <button>Delete</button>

                            </div>

                        </div>


                    </div>
                </div>
            </section>
        </div>
        <div style="height: 188px;width:100%;"></div>
        <footer>
            <?php include "./part/footer.php"?>
        </footer>
        <?php include "./part/go-top.php" ?>

    </div>
</body>

</html>