<?php ?>
<div class="go-top" onclick="topFunction()" id="myBtn">
            <img src="../image/home.png" alt="" style="width: 15px;">
        </div>
        <script>
            $(document).ready(function () {
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 125) {
                        $('.go-top').fadeIn();
                    } else {
                        $('.go-top').fadeOut();
                    }
                });
                //Click event to scroll to top
                $('.go-top').click(function () {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 800);
                    return false;
                });
            })
        </script>