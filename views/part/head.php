<?php ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" href="../image/logo.jpg">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/profile-user.css">
    
    <script src="../libs/jquery-3.3.1.js"></script>