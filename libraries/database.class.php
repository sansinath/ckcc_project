<?php
class Database {
    static function query($sql){
         //1. connect to db
        $con = mysqli_connect('localhost','root','','myproject');
        if(!$con){
            return false;
        }

        //2. run query and get result
        $result = mysqli_query($con, $sql);

        //3. close connection
        mysqli_close($con);
        
        return $result;
    }
}