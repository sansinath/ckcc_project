<?php
class View{
    public static function render($view, $data = []){    
        extract($data);
        include "../views/" .$view ."-view.php";    
    }
}